<?php
require '../Manager/SpecialiteManager.php';

$specialiteManager = new specialiteManager();

$tsspecialite = $specialiteManager->getAllspecialite();
setcookie('existspecialite','cette specialite existe déjà');
setcookie('nonexistspecialite','cette specialite est inexistante');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affichage table specialite</title>
</head>
<body>
<div class="container">
    <form action="affSpecialite.php" method="POST" enctype="multipart/form-data">
        <div class="input-group mb-3">
            <?php
            foreach ($tsspecialite as $specialite) {
                ?>

                <p href="#" id="<?= $specialite->getLibellespecialite() ?>">
                    <?= $specialite->getLibellespecialite(); ?>

                    <a class="dropdown-item ml2"
                       href="suppSpecialite.php?libelle_specialite=<?= $specialite->getLibellespecialite() ?>"
                       name="suppr">Suppression</a>

                </p>
                <?php
            }
            ?>
        </div>


        <div class="dropdown mt2">
            <!-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action choisie
            </button>  -->
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item ml2" href="ajoutSpecialite.php" name="ajout">Ajout</a>
                <a class="dropdown-item ml2" href="actions_admin.php" name="retour">Retour</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>
