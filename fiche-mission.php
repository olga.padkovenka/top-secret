<?php 
    require './Manager/MissionManager.php';

    $missionManager = new MissionManager();
    $missions = $missionManager->getByCode($_GET['codeMission']);
    //$missions = $missionManager->getByCode('APACHE');

    //$missions = $missionManager->getElementById(); // get by where 

    // $id = 'APACHE';
    // $database->query("SELECT * FROM missions WHERE code_mission = '$id'");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="index.css">
    <title>Classé Top Secret</title>
</head>
<body class="intro-2">

    <div class="container">
        <div class="row">
            <div class="col">
                <img class="mt-3" src="logo.png">
            </div>
            <div class="col">
                <h2 class="mt-5 text-white">Classé Top Secret</h2> 
            </div>
        </div>
    </div>
       
        <?php foreach ($missions as $mission)
        { 
    ?>
    <div class="container">
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-danger mt-3 mb-3 mr-3 ml-3 text-white" aria-haspopup="true">
            <a href="index-php.php">Voir d'autres missions</a>
            </button>
        </div>
    </div>    
</div>

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card" style="width: 12rem;">
                    <div class="card-header h5">
                    Mission: <?= $mission->getcodeMission(); ?>
                    </div>
                     <!--Mission -->
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Titre: <?= $mission->gettitreMission(); ?></li>
                        <li class="list-group-item">Description: <?= $mission->getdescriptionMission(); ?></li>
                        <li class="list-group-item">Début: <?= $mission->getdateDebutMission(); ?></li>
                        <li class="list-group-item">Fin: <?= $mission->getdateFinMission(); ?></li>
                        <li class="list-group-item">Statut: <?= $mission->getlibelleStatutMission(); ?></li>
                        <li class="list-group-item">Pays: <?= $mission->getcodePaysMission(); ?></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="card" style="width: 13rem;">
                    <div class="card-header h5">
                    Agent: <?= $mission->getagent()->getcodeAgent(); ?>
                    </div>
                    <ul class="list-group list-group-flush">
                         <!--Agent --> 
                        <li class="list-group-item"><?= $mission->getagent()->getprenomAgent(); ?> <?= $mission->getagent()->getnomAgent(); ?></li> 
                        <li class="list-group-item">Né(e): <?= $mission->getagent()->getdateNaissanceAgent(); ?></li>
                        <li class="list-group-item">Nationalité: <?= $mission->getagent()->getcodePaysAgent(); ?></li> 
                         <!--Agent specialite-->
                        <li class="list-group-item">Spécialité: <?= $mission->getspecialite()->getlibelleSpecialite(); ?></li>
                    </ul>
                </div>
            </div>
            <div class="col">
            <div class="card" style="width: 12rem;">
                    <div class="card-header h5">
                 Cible: <?= $mission->getcible()->getcodeCible();?>
                    </div>
                    <ul class="list-group list-group-flush">
                         <!--Cible -->
                        <li class="list-group-item"><?= $mission->getcible()->getprenomCible(); ?> <?= $mission->getcible()->getnomCible(); ?></li>
                        <li class="list-group-item">Né(e): <?= $mission->getcible()->getdateNaissanceCible(); ?></li>
                        <li class="list-group-item">Nationalité: <?= $mission->getcible()->getcodePaysCible(); ?></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                  <!--Planque -->
                <div class="card" style="width: 12rem;">
                    <div class="card-header h5">
                     Planque: <?= $mission->getplanque()->getcodePlanque(); ?>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Libellé: <?= $mission->getplanque()->getlibellePlanque(); ?></li>
                         <li class="list-group-item">Adresse: <?= $mission->getplanque()->getadressePlanque(); ?></li>
                        <li class="list-group-item">Pays: <?= $mission->getplanque()->getcodePaysPlanque(); ?></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <!--Contact -->
                <div class="card" style="width: 12rem;">
                    <div class="card-header h5">
                    Contacts: <?= $mission->getcontact()->getcodeContact(); ?>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><?= $mission->getcontact()->getprenomContact(); ?> <?= $mission->getcontact()->getnomContact(); ?></li>
                        <li class="list-group-item">Né(e): <?= $mission->getcontact()->getdateNaissanceContact(); ?></li>
                        <li class="list-group-item">Nationalité: <?= $mission->getcontact()->getcodePaysContact(); ?></li>
                    </ul>
                </div>
            </div>
        </div> <!--Fin row -->
    </div> <!--Fin container -->

    <div class="container">
        <div class="row">
           
        </div> <!--Fin row -->
    

    <?php
        }
    ?>

    </div> <!--Fin container -->
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>