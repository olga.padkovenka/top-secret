<?php

class Mission {
    private $codeMission; 
    private $titreMission;
    private $descriptionMission;
    private $dateDebutMission;
    private $dateFinMission;
    private $codePaysMission;
    private $libelleMission;
    private $libelleStatutMission;
    private $agent; //mes variables

    public function getcodeMission() { //fonction qui cherche des données de ma table Missions
        return $this->codeMission;
    }

    public function setcodeMission($codeMission) {
        $this->codeMission = $codeMission;
    }
    public function gettitreMission() {
        return $this->titreMission;
    }

    public function settitreMission($titreMission) {
        $this->titreMission = $titreMission;
    }

    public function getdescriptionMission() {
        return $this->descriptionMission;
    }

    public function setdescriptionMission($descriptionMission) {
        $this->descriptionMission = $descriptionMission;
    }
    public function getdateDebutMission() {
        return $this->dateDebutMission;
    }

    public function setdateDebutMission($dateDebutMission) {
        $this->dateDebutMission = $dateDebutMission;
    }
    public function getdateFinMission() {
        return $this->dateFinMission;
    }

    public function setdateFinMission($dateFinMission) {
        $this->dateFinMission = $dateFinMission;
    }

    public function getcodePaysMission() {
        return $this->codePaysMission;
    }
    public function setcodePaysMission($codePaysMission) {
        $this->codePaysMission = $codePaysMission;
    }
    public function getlibelleMission() {
        return $this->libelleMission;
    }
    public function setlibelleMission($libelleMission) {
        $this->libelleMission = $libelleMission;
    }
    public function getlibelleStatutMission() {
        return $this->libelleStatutMission;
    }
    public function setlibelleStatutMission($libelleStatutMission) {
        $this->libelleStatutMission = $libelleStatutMission;
    }

    public function getagent() {
        return $this->agent;
    }
    public function setagent($agent) {
        $this->agent = $agent;
    }

    public function getcible() {
        return $this->cible;
    }
    public function setcible($cible) {
        $this->cible = $cible;
    }

    public function getcontact() {
        return $this->contact;
    }
    public function setcontact($contact) {
        $this->contact = $contact;
    }

    public function getplanque() {
        return $this->planque;
    }
    public function setplanque($planque) {
        $this->planque = $planque;
    }

    public function getspecialite() {
        return $this->specialite;
    }
    public function setspecialite($specialite) {
        $this->specialite = $specialite;
    }

    public function getpays() {
        return $this->pays;
    }
    public function setpays($pays) {
        $this->pays = $pays;
    }

}