<?php

class Agent
{
    private $codeAgent;
    private $nomAgent;
    private $prenomAgent;
    private $dateNaissanceAgent;
    private $codePaysAgent; //mes variables
    private $libelleSpecialite;

    public function getcodeAgent()
    { //fonction qui cherche des données de ma table Missions
        return $this->codeAgent;
    }

    public function setcodeAgent($codeAgent)
    {
        $this->codeAgent = $codeAgent;
    }

    public function getnomAgent()
    { //fonction qui cherche des données de ma table Missions
        return $this->nomAgent;
    }

    public function setnomAgent($nomAgent)
    {
        $this->nomAgent = $nomAgent;
    }

    public function getprenomAgent()
    { //fonction qui cherche des données de ma table Missions
        return $this->prenomAgent;
    }

    public function setprenomAgent($prenomAgent)
    {
        $this->prenomAgent = $prenomAgent;
    }

    public function getdateNaissanceAgent()
    { //fonction qui cherche des données de ma table Missions
        return $this->dateNaissanceAgent;
    }

    public function setdateNaissanceAgent($dateNaissanceAgent)
    {
        $this->dateNaissanceAgent = $dateNaissanceAgent;
    }

    public function getcodePaysAgent() {
        return $this->codePaysAgent;
    }
    public function setcodePaysAgent($codePaysAgent) {
        $this->codePaysAgent = $codePaysAgent;
    }

    public function getlibelleSpecialite() {
        return $this->libelleSpecialite;
    }
    public function setlibelleSpecialite($libelleSpecialite) {
        $this->libelleSpecialite = $libelleSpecialite;
    }

}