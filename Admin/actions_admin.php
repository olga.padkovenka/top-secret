<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="../index.css">
    <title>Classé Top Secret</title>
</head>
<body class="intro-2">

    <div class="container">
        <div class="row">
            <div class="col">
                <img class="mt-3" src="../logo.png">
            </div>
            <div class="col">
                <h1 class="mt-5 text-white">Classé Top Secret</h1> 
            </div>
        </div>
    </div>

    <div class="container">
    <form action="actions_admin.php" method="POST" enctype="multipart/form-data">
     <div class="input-group mb-3">

        <select name='table' class="custom-select" id="inputGroupSelect01">
            <option selected>Choisissez votre table...</option>
            <option id='missions' value="1">missions</option>
            <option id='contact' value="2">contact</option>
            <option id='agents' value="3">agents</option>
            <option id='cibles' value="4">cibles</option>
            <option id='pays' value="5">pays</option>
            <option id='planques' value="6">planques</option>
            <option id='type_mission' value="7">type_mission</option>
            <option id='type_planque' value="8">type_planque</option>
            <option id='admin' value="9" href="affAdmin.php">admin</option>
            <option id='specialite' value="10" href="affSpecialite.php">specialites</option>
        </select>
     </div>

     <button class="col-sm-3 btn btn-danger text-white" id="envoyer" type="submit">envoyer</button>
    </form>
    
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>

<?php

        if (isset($_POST['table'])) {
            $table = ceil($_POST['table']);
        } else {
            $table = 1;
        }

        switch ($table) {
            case 1:
                     break;
            case 2:
                ?> <a href="affcontact.php"></a> <?php
                    break;
            case 3:
                header('Location: affAgent.php');
                    break;
            case 4:
                ?> <a href="affcible.php"></a> <?php
                        break;
            case 5:
                header('Location: affPays.php');
                 break;
            case 6:
                ?> <a href="affplanque.php"></a> <?php
                    break;
            case 7:
                ?> <a href="afftypemission.php"></a> <?php
                    break;
            case 8:
                ?> <a href="afftypeplanque.php">Type planque</a> <?php
                    break;
            case 9:
                header('Location: affAdmin.php');
                break;

       }

      ?>
