<?php

class Contact
{
    private $codeContact;
    private $codePaysContact;
    private $dateNaissanceContact;
    private $nomContact; 
    private $prenomContact; //mes variables

    public function getcodeContact()
    { //fonction qui cherche des données de ma table Missions
        return $this->codeContact;
    }

    public function setcodeContact($codeContact)
    {
        $this->codeContact = $codeContact;
    }

    public function getcodePaysContact()
    { //fonction qui cherche des données de ma table Missions
        return $this->codePaysContact;
    }

    public function setcodePaysContact($codePaysContact)
    {
        $this->codePaysContact = $codePaysContact;
    }

    public function getdateNaissanceContact()
    { //fonction qui cherche des données de ma table Missions
        return $this->dateNaissanceContact;
    }

    public function setdateNaissanceContact($dateNaissanceContact)
    {
        $this->dateNaissanceContact = $dateNaissanceContact;
    }

    public function getnomContact()
    { //fonction qui cherche des données de ma table Missions
        return $this->nomContact;
    }

    public function setnomContact($nomContact)
    {
        $this->nomContact = $nomContact;
    }

    public function getprenomContact() {
        return $this->prenomContact;
    }
    public function setprenomContact($prenomContact) {
        $this->prenomContact = $prenomContact;
    }

}