<?php

class Planque
{
    private $adressePlanque; //create table
    private $codePaysPlanque;
    private $codePlanque;
    private $libellePlanque;

    public function getAll(){
        return array();
    }

    public function getadressePlanque()
    {
        return $this->adressePlanque;
    }

    public function setadressePlanque($adressePlanque)
    {
        $this->adressePlanque = $adressePlanque;
    }

    public function getcodePaysPlanque()
    {
        return $this->codePaysPlanque;
    }

    public function setcodePaysPlanque($codePaysPlanque)
    {
        $this->codePaysPlanque = $codePaysPlanque;
    }

    public function getcodePlanque()
    {
        return $this->codePlanque;
    }

    public function setcodePlanque($codePlanque)
    {
        $this->codePlanque = $codePlanque;
    }

    public function getlibellePlanque()
    {
        return $this->libellePlanque;
    }

    public function setlibellePlanque($libellePlanque)
    {
        $this->libellePlanque = $libellePlanque;
    }
}