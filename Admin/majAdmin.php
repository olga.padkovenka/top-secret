<?php

require '../Manager/AdminManager.php';
$newadminManager = new AdminManager();
$majadminManager = new AdminManager();
$connexion = new PDO("mysql:host=localhost;dbname=catalogue_top_secret;port=8889;charset=utf8mb4", "root", "root");
$code= $_GET['code'];
$sql =  "SELECT * FROM admins WHERE code_admin = '$code'";
$stmt = $connexion->query($sql);
//fetch 1 admin

$row = $stmt->fetch();

if ($row) {
   $nom = $row['nom_admin'];
   $prenom= $row['prenom_admin'];
   $mail =  $row['adresse_email_admin'];
   $pwd = $row['pwd_admin'];
   $date = $row['date_creat_admin'];
}

if (isset($_POST['adresse_email_admin'],
        $_POST['pwd_admin'],
        $_POST['date_creat_admin'])) {

        echo('modif ok');
        $admin = new Admin;
        echo($_POST['nom_admin']);
        $admin->setcodeAdmin($_POST['code_admin']);
        $admin->setnomAdmin($_POST['nom_admin']);
        $admin->setprenomAdmin($_POST['prenom_admin']);
        $admin->setadresseEmailAdmin($_POST['adresse_email_admin']);
        $admin->setpwdAdmin($_POST['pwd_admin']);
        $admin->setdateCreatAdmin($_POST['date_creat_admin']);
        $majadmin = $majadminManager->majAdmin($admin);
        var_dump($majadmin);
        header('Location: affAdmin.php');

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajout </title>
</head>
<body>
<div class="container">
    <h1>Table Admin </h1>
    <form action="majAdmin.php" method="POST" enctype="multipart/form-data">

        <input class="ml-5 col-sm-6" id="code_admin" type="text" name="code_admin"
               placeholder="code nouvel admin" value="<?=$code?>"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="nom_admin" type="text" name="nom_admin"
               placeholder="nom nouvel admin" value="<?=$nom?>"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="prenom_admin" type="text" name="prenom_admin"
               placeholder="prenom nouvel admin" value="<?=$prenom?>"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="adresse_email_admin" type="email" name="adresse_email_admin"
               placeholder="votre identifiant email"
        value="<?=$mail?>" autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="pwd_admin" type="text" name="pwd_admin" placeholder="rentrez votre mot de passe"
         value = "<?=$pwd?>"      autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="date_creat_admin" type="text" name="date_creat_admin"
               placeholder="rentrez la date de création" value = "<?=$date?>">

        <button class="col-sm-3 btn btn-outline-primary" id="valider" type="submit">Modifier</button>
    </form>
</div>
</body>
</html>
