<?php

require '../Manager/AgentManager.php';
// require './manager/DBManager.php';
$newagentManager = new AgentManager();
$majagentManager = new AgentManager();
$connexion = new PDO("mysql:host=localhost;dbname=catalogue_top_secret;port=8889;charset=utf8mb4", "root", "root");

$code= $_GET['code'];
$sql =  "SELECT * FROM agents 
JOIN possede_specialites ON agents.code_agent = possede_specialites.code_agent
WHERE agents.code_agent = '$code'";
$stmt = $connexion->query($sql);

$row = $stmt->fetch();

if ($row) {
    $nom = $row['nom_agent'];
    $prenom= $row['prenom_agent'];
    $date =  $row['date_naissance_agent'];
    $pays = $row['code_pays_agent'];
    $specialite = $row['libelle_specialite'];
}

if (isset($_POST['nom_agent'],
    $_POST['prenom_agent'],
    $_POST['date_naissance_agent'])) {

    echo('modif ok');
    $agent = new Agent;
    echo($_POST['nom_agent']);
    $agent->setcodeAgent($_POST['code_agent']);
    $agent->setnomAgent($_POST['nom_agent']);
    $agent->setprenomAgent($_POST['prenom_agent']);
    $agent->setdateNaissanceAgent($_POST['date_naissance_agent']);
    $agent->setcodePaysAgent($_POST['code_pays_agent']);
    $agent->setlibelleSpecialite($_POST['libelle_specialite']);
    $majagent = $majagentManager->majAgent($agent);
    var_dump($majagent);
    header('Location: affAgent.php');
}

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Modifier </title>
    </head>
    <body>
    <div class="container">
        <h1>Table Agent </h1>
        <form action="majAgent.php" method="POST" enctype="multipart/form-data">

            <input class="ml-5 col-sm-6" id="code_agent" type="text" name="code_agent"
                   placeholder="code nouvel agent" value="<?=$code?>"
                   autocomplete="off" required>
            <input class="ml-5 col-sm-6" id="nom_agent" type="text" name="nom_agent"
                   placeholder="nom nouvel agent" value="<?=$nom?>"
                   autocomplete="off" required>
            <input class="ml-5 col-sm-6" id="prenom_agent" type="text" name="prenom_agent"
                   placeholder="prenom nouvel agent" value="<?=$prenom?>"
                   autocomplete="off" required>
            <input class="ml-5 col-sm-6" id="date_naissance_agent" type="date" name="date_naissance_agent"
                   placeholder="date de naissance"
                   value="<?=$date?>" autocomplete="off" required>
            <input class="ml-5 col-sm-6" id="code_pays_agent" type="text" name="code_pays_agent" placeholder="code pays"
                   value = "<?=$pays?>"      autocomplete="off" required>
            <input class="ml-5 col-sm-6" id="libelle_specialite" type="text" name="libelle_specialite" placeholder="libelle_specialite"
                   value = "<?=$specialite?>" autocomplete="off" required>
            <button class="col-sm-3 btn btn-outline-primary" name="retour"  href="affAgent.php" type="submit">Retour</button>
            <button class="col-sm-3 btn btn-outline-primary" id="valider" type="submit">Modifier</button>
        </form>
    </div>
    </body>
    </html>
