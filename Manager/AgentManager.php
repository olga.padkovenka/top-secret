<?php

require '../Manager/DBManager.php';
require '../Model/Agent.php';

class AgentManager extends DBManager{
    public function getAllAgent() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * 
        FROM agents a
        INNER JOIN possede_specialites p
        ON a.code_agent = p.code_agent'
);

        while($row = $stmt->fetch()) {
            $agent = new Agent(); //je fais un nouveau agent
            $agent->setcodeAgent($row['code_agent']);
            $agent->setnomAgent($row['nom_agent']);
            $agent->setprenomAgent($row['prenom_agent']);
            $agent->setdateNaissanceAgent($row['date_naissance_agent']);
            $agent->setcodePaysAgent($row['code_pays_agent']);
            $agent->setLibelleSpecialite($row['libelle_specialite']);

            $result[] = $agent;
        }
        return $result;
    }

    public function getidentAgent($code_agent) {
        $agent = null;

        $stmt = $this->getConnexion()->prepare('SELECT * FROM agents
         WHERE code_agent = :code_agent');
        $stmt->execute(['code_agent' => $code_agent]);
        var_dump($stmt);
        $row = $stmt->fetch();

        if ($row) {
            $agent = new Agent();
            $agent->setcodeAgent($row['code_agent']);
            $agent->setnomAgent($row['nom_agent']);
            $agent->setprenomAgent($row['prenom_agent']);
            $agent->setdateNaissanceAgent($row['date_naissance_agent']);
            $agent->setcodePaysAgent($row['code_pays_agent']);
        };
        return $agent;
    }
    public function addAgent(Agent $agent) {

        $code = $agent->getcodeAgent();
        $nom = $agent->getnomAgent();
        $prenom = $agent->getprenomAgent();
        $pays = $agent->getcodePaysAgent();
        $date = $agent->getdateNaissanceAgent();
        $specialite = $agent->getLibelleSpecialite();

        $sql = "INSERT INTO agents 
         VALUES ('$code', '$nom', '$prenom', '$date', '$pays')";

        $stmt = $this->getConnexion()->query($sql);
        $sql = "INSERT INTO  possede_specialites
         VALUES ('$code', '$specialite')";

        $stmt = $this->getConnexion()->query($sql);
        return $stmt;
    }

    public function majAgent(Agent $agent) {
        $code = $agent->getcodeAgent();
        $nom= $agent->getnomAgent();
        $prenom = $agent->getprenomAgent();
        $datenaiss = $agent->getdateNaissanceAgent();
        $pays = $agent->getcodePaysAgent();

        $sql = "UPDATE agents 
        set nom_agent = '$nom',
            prenom_agent = '$prenom',
            date_naissance_agent = '$datenaiss',
            code_pays = '$pays'
            WHERE code_agent = '$code'";

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;

    }
    public function supAgent($code) {

        $sql = "DELETE FROM agents 
        WHERE code_agent = '$code'";

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;

    }

}

    
