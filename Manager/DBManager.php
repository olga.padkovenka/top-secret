<?php

class DBManager {
    private $connexion;

    public function __construct()
    {
        $this->connexion = new PDO("mysql:host=localhost;dbname=catalogue_top_secret;port=8889;charset=utf8mb4", "root", "root");
    }

    public function getConnexion() {
        return $this->connexion;
    }
}