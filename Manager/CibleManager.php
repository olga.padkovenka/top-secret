<?php

require './Manager/DBManager.php';
require './Model/Cible.php';

class AgentManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM cibles');

        while($row = $stmt->fetch()) {
            $cible = new Cible();
            $cible->setcodeCible($row['code_cible']);
            $cible->setcodeMission($row['code_mission']);
            $cible->setcodePaysCible($row['code_pays_cible']);
            $cible->setdateNaissanceCible($row['date_naissance_cible']);
            $cible->setnomCible($row['nom_pays']);
            $cible->setprenomCible($row['prenom_pays']);

            $result[] = $cible;
        }

        return $result;

    }

   
    
}