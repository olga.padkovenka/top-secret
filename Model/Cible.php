<?php

class Cible
{
    private $codeCible;
    private $codeMission;
    private $codePaysCible;
    private $dateNaissanceCible;
    private $nomCible;
    private $prenomCible;
     //mes variables

    public function getcodeCible()
    { //fonction qui cherche des données de ma table Missions
        return $this->codeCible;
    }

    public function setcodeCible($codeCible)
    {
        $this->codeCible = $codeCible;
    }

    public function getcodeMission()
    { //fonction qui cherche des données de ma table Missions
        return $this->codeMission;
    }

    public function setcodeMission($codeMission)
    {
        $this->codeMission = $codeMission;
    }

    public function getcodePaysCible()
    { //fonction qui cherche des données de ma table Missions
        return $this->codePaysCible;
    }

    public function setcodePaysCible($codePaysCible)
    {
        $this->codePaysCible = $codePaysCible;
    }

    public function getdateNaissanceCible()
    { //fonction qui cherche des données de ma table Missions
        return $this->dateNaissanceCible;;
    }

    public function setdateNaissanceCible($dateNaissanceCible)
    {
        $this->dateNaissanceCible = $dateNaissanceCible;
    }

    public function getnomCible() {
        return $this->nomCible;
    }
    public function setnomCible($nomCible) {
        $this->nomCible = $nomCible;
    }

    public function getprenomCible() {
        return $this->prenomCible;
    }
    public function setprenomCible($prenomCible) {
        $this->prenomCible = $prenomCible;
    }

}