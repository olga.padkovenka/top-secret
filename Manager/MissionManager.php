<?php

require './Manager/DBManager.php';
require './Model/Mission.php';
require './Model/Agent.php';
require './Model/Cible.php';
require './Model/Contact.php';
require './Model/Planque.php';
require './Model/Specialite.php';
require './Model/Pays.php';

class MissionManager extends DBManager
{
    public function getAll()
    {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM missions 
        JOIN agents_effectuer_missions ON agents_effectuer_missions.code_mission = missions.code_mission 
        JOIN agents ON agents_effectuer_missions.code_agent = agents.code_agent
        JOIN cibles ON cibles.code_mission = missions.code_mission
        JOIN contacts_lier_missions ON contacts_lier_missions.code_mission = missions.code_mission
        JOIN contacts ON contacts.code_contact = contacts_lier_missions.code_contact
        JOIN attribuer_missions ON attribuer_missions.code_mission = missions.code_mission
        JOIN planques ON planques.code_planque = attribuer_missions.code_planque
        JOIN possede_specialites ON possede_specialites.code_agent = agents.code_agent
        JOIN specialites ON specialites.libelle_specialite = possede_specialites.libelle_specialite
        JOIN pays ON pays.code_pays = missions.code_pays_mission;');
        
        // JOINTURES a vérifier
        // $stmt = $this->getConnexion()->query('SELECT * FROM missions JOIN agents_effectuer_missions ON agents_effectuer_missions.code_mission = missions.code_mission');

        // si besoin pour info
        // $agts = $this->getConnexion()->query('SELECT * FROM agents_effectuer_missions');

        while ($row = $stmt->fetch()) {
            $mission = new Mission();
            $mission->setcodeMission($row['code_mission']);
            $mission->settitreMission($row['titre_mission']);
            $mission->setdescriptionMission($row['descriptions']);
            $mission->setdateDebutMission($row['date_debut_mission']);
            $mission->setdateFinMission($row['date_fin_mission']);
            $mission->setlibelleStatutMission($row['libelle_statut']);
            $mission->setcodePaysMission($row['code_pays_mission']);

            $agent = new Agent();
            $agent->setcodeAgent($row['code_agent']);
            $agent->setnomAgent($row['nom_agent']);
            $agent->setprenomAgent($row['prenom_agent']);
            $agent->setdateNaissanceAgent($row['date_naissance_agent']);
            $agent->setcodePaysAgent($row['code_pays_agent']);
            $mission->setagent($agent);

            $cible = new Cible();
            $cible->setcodeCible($row['code_cible']);
            $cible->setcodeMission($row['code_mission']);
            $cible->setcodePaysCible($row['code_pays_cible']);
            $cible->setdateNaissanceCible($row['date_naissance_cible']);
            $cible->setnomCible($row['nom_cible']);
            $cible->setprenomCible($row['prenom_cible']);
            $mission->setcible($cible);

            $contact = new Contact();
            $contact->setcodeContact($row['code_contact']);
            $contact->setcodePaysContact($row['code_pays_contact']);
            $contact->setdateNaissanceContact($row['date_naissance_contact']);
            $contact->setnomContact($row['nom_contact']);
            $contact->setprenomContact($row['prenom_contact']);
            $mission->setcontact($contact);

            $planque = new Planque();
            $planque->setadressePlanque($row['adresse_planque']);
            $planque->setcodePaysPlanque($row['code_pays_planque']);
            $planque->setcodePlanque($row['code_planque']);
            $planque->setlibellePlanque($row['libelle_planque']);
            $mission->setplanque($planque);

            $specialite = new Specialite();
            $specialite->setlibelleSpecialite($row['libelle_specialite']);
            $mission->setspecialite($specialite);

            $result[] = $mission;

            //type de mission
        }

        return $result;
    }

    public function getByCode($codeMission)
    {
        $result = [];

        $stmt = $this->getConnexion()->query("SELECT * FROM missions 
        JOIN agents_effectuer_missions ON agents_effectuer_missions.code_mission = missions.code_mission 
        JOIN agents ON agents_effectuer_missions.code_agent = agents.code_agent
        JOIN cibles ON cibles.code_mission = missions.code_mission 
        JOIN contacts_lier_missions ON contacts_lier_missions.code_mission = missions.code_mission
        JOIN contacts ON contacts.code_contact = contacts_lier_missions.code_contact
        JOIN attribuer_missions ON attribuer_missions.code_mission = missions.code_mission
        JOIN planques ON planques.code_planque = attribuer_missions.code_planque
        JOIN possede_specialites ON possede_specialites.code_agent = agents.code_agent
        JOIN specialites ON specialites.libelle_specialite = possede_specialites.libelle_specialite
        JOIN pays ON pays.code_pays = missions.code_pays_mission
        WHERE missions.code_mission = '$codeMission'"); //pour chercher par une mission par son code_mission (id)

        while ($row = $stmt->fetch()) {
            $mission = new Mission();
            $mission->setcodeMission($row['code_mission']);
            $mission->settitreMission($row['titre_mission']);
            $mission->setdescriptionMission($row['descriptions']);
            $mission->setdateDebutMission($row['date_debut_mission']);
            $mission->setdateFinMission($row['date_fin_mission']);
            $mission->setlibelleStatutMission($row['libelle_statut']);
            $mission->setcodePaysMission($row['code_pays_mission']);

            $agent = new Agent();
            $agent->setcodeAgent($row['code_agent']);
            $agent->setnomAgent($row['nom_agent']);
            $agent->setprenomAgent($row['prenom_agent']);
            $agent->setdateNaissanceAgent($row['date_naissance_agent']);
            $agent->setcodePaysAgent($row['code_pays_agent']);
            $mission->setagent($agent);

            $cible = new Cible();
            $cible->setcodeCible($row['code_cible']);
            $cible->setcodeMission($row['code_mission']);
            $cible->setcodePaysCible($row['code_pays_cible']);
            $cible->setdateNaissanceCible($row['date_naissance_cible']);
            $cible->setnomCible($row['nom_cible']);
            $cible->setprenomCible($row['prenom_cible']);
            $mission->setcible($cible);

            $contact = new Contact();
            $contact->setcodeContact($row['code_contact']);
            $contact->setcodePaysContact($row['code_pays_contact']);
            $contact->setdateNaissanceContact($row['date_naissance_contact']);
            $contact->setnomContact($row['nom_contact']);
            $contact->setprenomContact($row['prenom_contact']);
            $mission->setcontact($contact);

            $planque = new Planque();
            $planque->setadressePlanque($row['adresse_planque']);
            $planque->setcodePaysPlanque($row['code_pays_planque']);
            $planque->setcodePlanque($row['code_planque']);
            $planque->setlibellePlanque($row['libelle_planque']);
            $mission->setplanque($planque);

            $specialite = new Specialite();
            $specialite->setlibelleSpecialite($row['libelle_specialite']);
            $mission->setspecialite($specialite);

            $result[] = $mission;
        }
        return $result;
    }
}