<?php

require './Manager/DBManager.php';
require './Model/Contact.php';

class AgentManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM contacts');

        while($row = $stmt->fetch()) {
            $cible = new Contact();
            $cible->setcodeContact($row['code_contact']);
            $cible->setcodePaysContact($row['code_pays_contact']);
            $cible->setdateNaissanceContact($row['date_naissance_contact']);
            $cible->setnomContact($row['nom_contact']);
            $cible->setprenomContact($row['prenom_contact']);

            $result[] = $cible;
        }

        return $result;

    }

   
    
}